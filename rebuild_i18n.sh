#! /bin/sh

#LOCALES=`find . -type d | grep -m 1 "locales"`
#SOURCE=`dirname $LOCALES`;

## rebuild pot file for package's domain and merge it with any manual translations needed
#bin/i18ndude rebuild-pot --pot $LOCALES/$I18NDOMAIN.pot --merge $LOCALES/manual.pot --create $I18NDOMAIN $SOURCE
#
## synchronise translations for package's domain
#for po in $LOCALES/*/LC_MESSAGES/$I18NDOMAIN.po; do
#    bin/i18ndude sync --pot $LOCALES/$I18NDOMAIN.pot $po
#done
#
## rebuild pot file for Plone's domain
#bin/i18ndude rebuild-pot --pot $LOCALES/plone.pot --create plone $SOURCE/configure.zcml $SOURCE/profiles/default
#
## synchronise translations for Plone's domain
#for po in $LOCALES/*/LC_MESSAGES/plone.po; do
#    bin/i18ndude sync --pot $LOCALES/plone.pot $po
#done



I18NDOMAIN="codevasf.webservice"
BASE_DIRECTORY="src/$I18NDOMAIN/src/codevasf.webservice"

# Synchronise the templates and scripts with the .pot.
bin/i18ndude rebuild-pot --pot ${BASE_DIRECTORY}/locales/${I18NDOMAIN}.pot \
    --merge ${BASE_DIRECTORY}/locales/manual.pot \
    --create ${I18NDOMAIN} \
    ${BASE_DIRECTORY}

# Synchronise the resulting .pot with all .po files
for po in ${BASE_DIRECTORY}/locales/*/LC_MESSAGES/${I18NDOMAIN}.po; do
    echo "PASSA"
    bin/i18ndude sync --pot ${BASE_DIRECTORY}/locales/${I18NDOMAIN}.pot $po
done

# Synchronise the templates and scripts with the .pot.
bin/i18ndude rebuild-pot --pot ${BASE_DIRECTORY}/locales/plone.pot \
    --create plone \
    ${BASE_DIRECTORY}/configure.zcml \
    ${BASE_DIRECTORY}/profiles/default

# Synchronise the Plone's pot file (Used for the workflows)
for po in ${BASE_DIRECTORY}/locales/*/LC_MESSAGES/plone.po; do
    bin/i18ndude sync --pot ${BASE_DIRECTORY}/locales/plone.pot $po
done

# Report of errors and suspect untranslated messages
bin/i18ndude find-untranslated -n ${BASE_DIRECTORY}
