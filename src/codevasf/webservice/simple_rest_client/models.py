# -*- coding: utf-8 -*-
from collections import namedtuple

Request = namedtuple(
    'Request', ['url', 'method', 'params', 'body', 'headers', 'timeout', 'verify']
)
Response = namedtuple(
    'Response', ['url', 'method', 'body', 'headers', 'status_code']
)
