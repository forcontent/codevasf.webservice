# -*- coding: utf-8 -*-
# flake8: noqa

from zope.i18nmessageid import MessageFactory
from config import PROJECTNAME

_ = MessageFactory(PROJECTNAME)


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
