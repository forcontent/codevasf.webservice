# -*- coding: utf-8 -*-

PROJECTNAME = 'codevasf.webservice'


def sandBoxUri(sandbox=False):
    """ SandBox Test or Production URIs """
    API_URI = ''
    if sandbox:
        API_URI = 'https://www.mocky.io/v2/'
    return API_URI
