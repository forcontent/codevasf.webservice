# -*- coding: utf-8 -*-

import os
from plone import api

from codevasf.webservice.tests.base import INTEGRATION_TESTING
from codevasf.webservice.api import get_api_instance
from codevasf.webservice import config
from codevasf.webservice import exceptions

import unittest


class TestConveniosContratos(unittest.TestCase):
    """Test Convênios e Contratos"""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.apiCodevasf = get_api_instance(token='', sandbox=True)
        self.app = self.layer['app']
        self.portal = api.portal.get()
        self.request = self.layer['request']
