# -*- coding: utf-8 -*-

import os
import status
from plone import api

from codevasf.webservice.tests.base import INTEGRATION_TESTING
from codevasf.webservice.api import get_api_instance
from codevasf.webservice import config
from codevasf.webservice import exceptions
from codevasf.webservice.tests.vcr_config import vcr

import unittest


class TestApi(unittest.TestCase):
    """Test API"""

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.apiCodevasf = get_api_instance(token='', sandbox=True)
        self.app = self.layer['app']
        self.portal = api.portal.get()
        self.request = self.layer['request']

    def test_get_api_instance(self):
        print('\n')
        print('*' * 8 + ' Running with api_root_url: {} '.format(self.apiCodevasf.api_root_url) + 8 * '*')
        self.assertNotEqual(self.apiCodevasf.api_root_url, '')
        self.assertEqual(self.apiCodevasf.headers.get('Content-Type'), 'application/json')
        self.assert_(self.apiCodevasf.json_encode_body)

    def test_get_api_instance_with_api_root_url(self):
        uri_foo = 'https://foo.com'
        self.apiCodevasf = get_api_instance(token='',
                                            sandbox=False,
                                            api_root_url=uri_foo)
        self.assertNotEqual(self.apiCodevasf.api_root_url, '')
        self.assertEqual(self.apiCodevasf.api_root_url, uri_foo)
        self.assertEqual(self.apiCodevasf.headers.get('Content-Type'), 'application/json')
        self.assert_(self.apiCodevasf.json_encode_body)

    @vcr.use_cassette()
    def test_api(self):
        """ """
        response = self.apiCodevasf.test_api.list()
