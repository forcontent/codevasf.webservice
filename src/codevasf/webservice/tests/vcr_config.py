# -*- coding: utf-8 -*-
from os import path
from vcr import VCR

""" https://vcrpy.readthedocs.io/en/latest/usage.html """

dirname = path.join(path.dirname(__file__), 'resources/cassettes')
vcr = VCR(
    cassette_library_dir=dirname,
    path_transformer=VCR.ensure_suffix('.yaml'),
    record_mode='once',
    match_on=['method', 'path', 'query'],
)
