# -*- coding: utf-8 -*-
from codevasf.webservice.simple_rest_client.api import API
from codevasf.webservice.api import resource
from codevasf.webservice.config import sandBoxUri


def get_api_instance(token='', api_root_url=None, timeout=3, sandbox=False):
    headers = {
        'Content-Type': 'application/json'
    }
    import_batches_headers = {
        'Content-Type': 'multipart/form-data'
    }
    use_sandbox = sandbox
    api_root_url = api_root_url or sandBoxUri(sandbox)
    api = API(
        api_root_url=api_root_url, headers=headers, json_encode_body=True,
        timeout=timeout
    )
    # Usado para testar a API
    api.add_resource(
        resource_name='test_api',
        resource_class=resource.TesteApiResource
    )

    # Convênios e Contratos
    api.add_resource(
        resource_name='agreements_contracts',
        resource_class=resource.AgreementsContractsResource
    )

    # Consulta Exercicios
    api.add_resource(
        resource_name='exercises',
        resource_class=resource.ExerciciosResource
    )

    # Exercicios detalhes
    api.add_resource(
        resource_name='exercise_detail',
        resource_class=resource.ExerciciosDetailResource
    )

    # Consulta de Processos
    api.add_resource(
        resource_name='process_consult',
        resource_class=resource.ConsultaProcessosResource
    )

    # Consulta de Processos
    api.add_resource(
        resource_name='contests_selections',
        resource_class=resource.ContestsSelectionsResource
    )

    # Licitações Unidades
    api.add_resource(
        resource_name='bidding_units',
        resource_class=resource.BiddingUnitsResource
    )

    api.add_resource(
        resource_name='bidding_filters',
        resource_class=resource.BiddingFiltersResource
    )

    api.add_resource(
        resource_name='biddings',
        resource_class=resource.BiddingsResource
    )

    return api
