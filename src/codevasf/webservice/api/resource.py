from codevasf.webservice.simple_rest_client.resource import Resource


class TesteApiResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5185415ba171ea3a00704eed'
        },
        'create': {
            'method': 'POST',
            'url': '5185415ba171ea3a00704eed'
        },
        'retrieve': {
            'method': 'GET',
            'url': '5185415ba171ea3a00704eed/{}',
        },
        'update': {
            'method': 'PUT',
            'url': '5185415ba171ea3a00704eed/{}',
        },
    }


class AgreementsContractsResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5b6716c2320000600dee1232'
        },
    }


class ExerciciosResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5b68a5443300008b3a32dd2f'
        }
    }


class ExerciciosDetailResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5b68c7ee330000dd3d32dd93'
        }
    }


class ConsultaProcessosResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5b789d3d2e00005f00864cc6'
        }
    }


class ContestsSelectionsResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5b78b0d62e00005000864cd0'
        }
    }


class BiddingUnitsResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5b78d2ce2e00007000c02881'
        }
    }


class BiddingFiltersResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5bc379e83100006e001fcbd1'
        }
    }


class BiddingsResource(Resource):
    actions = {
        'list': {
            'method': 'GET',
            'url': '5bd250e32f0000574ad8ffec'
        }
    }
