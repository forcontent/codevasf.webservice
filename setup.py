# -*- coding: utf-8 -*-
import codecs
import os
import re

from setuptools import setup, find_packages, Command

here = os.path.abspath(os.path.dirname(__file__))

version = '0.0.0'
changes = os.path.join(here, 'CHANGES.rst')
match = r'^#*\s*(?P<version>[0-9]+\.[0-9]+(\.[0-9]+)?)$'
with codecs.open(changes, encoding='utf-8') as changes:
    for line in changes:
        res = re.match(match, line)
        if res:
            version = res.group('version')
            break

# Get the long description
with codecs.open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# Get version
with codecs.open(os.path.join(here, 'CHANGES.rst'), encoding='utf-8') as f:
    changelog = f.read()


install_requirements = [
    'decorator',
    'plone.api',
    'requests>=2.13.0',
    'requests[socks]',
    'json-encoder>=0.4.4',
    'simplejson',
    'python-status>=1.0.1',
    'pyjsonq>=1.0.2',
    'future>=0.13.1',
    'zope.globalrequest',
    'zope.component>=3.9.5',
]
tests_requirements = [
    'vcrpy>=1.13.0',
    'plone.app.testing',
    'plone.browserlayer',
    'plone.testing',
    'plone.app.robotframework',
]
develop_requires = install_requirements + [
    'Sphinx',
    'epydoc',
    'coverage',
    'flake8',
    'manuel',
    'zest.releaser',
]


class VersionCommand(Command):
    description = 'print library version'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        print(version)


setup(
    name='codevasf.webservice',
    version=version,
    description='Integração com API do Portal da Codevasf',
    long_description=long_description,
    url='https://bitbucket.org/forcontent/codevasf.webservice.git',
    author='forContent',
    author_email='suporte@forcontent.com.br',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Plone',
        'Framework :: Plone :: 4.3',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    keywords='rest client http brasil plone gov',
    packages=find_packages('src', exclude=['docs', 'tests*']),
    package_dir={'': 'src'},
    include_package_data=True,
    namespace_packages=['codevasf'],
    setup_requires=['pytest-runner'],
    zip_safe=False,
    install_requires=install_requirements,
    extras_require={
        'develop': develop_requires,
        'test': tests_requirements,
    },
    tests_require=tests_requirements,
    test_suite="codevasf.webservice",
    entry_points="""
      [z3c.autoinclude.plugin]
      target = plone
      """,
    cmdclass={
        'version': VersionCommand,
    },
)
